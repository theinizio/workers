# workers

Web page to control workers  

    cp .env.dist .env \
    && cp backend/.env.example backend/.env \
    && cp front/.env.example front/.env \
    && make up \
    && make populate_db

 Then visit http://localhost

