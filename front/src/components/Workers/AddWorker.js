import React, {useState} from "react";
import WorkerService from "../../services/worker.service";

const AddWorker = () => {

    const positions = [
            'Chief People Officer',
            'VP of Miscellaneous Stuff',
            'Chief Robot Whisperer',
            'Director of First Impressions',
            'Culture Operations Manager',
            'Director of Ethical Hacking',
            'Software Ninjaneer',
            'Director of Bean Counting',
            'Digital Overlord',
            'Director of Storytelling',
        ].map(position => <option>{position}</option>);

    const genders =  [
            'Female',
            'Male',
        ].map(gender => <option>{gender}</option>);

    const initialWorkerState = {
        _id: null,
        name: "",
        gender: "",
        salary: 0,
        position: "",
        phone: "",
        email: "",
    };
    const [worker, setWorker] = useState(initialWorkerState);
    const [submitted, setSubmitted] = useState(false);

    const handleInputChange = event => {
        const {name, value} = event.target;
        setWorker({...worker, [name]: value});
    };

    const saveWorker = async () => {
        const data = {
            name: worker.name,
            gender: worker.gender,
            salary: worker.salary,
            position: worker.position,
            phone: worker.phone,
            email: worker.email,
            addedAt: new Date(),
        };
        try {
            const response = await WorkerService.create(data)
            setWorker({
                _id: response.data._id,
                name: response.data.name,
                gender: response.data.gender,
                salary: response.data.salary,
                position: response.data.position,
                phone: response.data.phone,
                email: response.data.email,
            });
            setSubmitted(true);
            console.log(response.data);
        } catch (e) {
            console.log(e);
        }
    };

    const newWorker = () => {
        setWorker(initialWorkerState);
        setSubmitted(false);
    };

    return (
        <div className="submit-form">
            {submitted ? (
                <div>
                    <h4>You submitted successfully!</h4>
                    <button className="btn btn-success" onClick={newWorker}>
                        Add
                    </button>
                </div>
            ) : (
                <div>
                    <div className="form-group">
                        <label htmlFor="title">Name</label>
                        <input
                            type="text"
                            className="form-control"
                            id="name"
                            required
                            value={worker.name}
                            onChange={handleInputChange}
                            name="name"
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Gender</label>
                        <select
                            className="form-control"
                            id="gender"
                            required
                            value={worker.gender}
                            onChange={handleInputChange}
                            name="gender"
                        >{genders}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="title">Salary</label>
                        <input
                            type="number"
                            className="form-control"
                            id="salary"
                            required
                            value={worker.salary}
                            onChange={handleInputChange}
                            name="salary"
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Position</label>
                        <select
                            className="form-control"
                            id="position"
                            required
                            value={worker.position}
                            onChange={handleInputChange}
                            name="position"
                        >{positions}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="title">Phone</label>
                        <input
                            type="tel"
                            className="form-control"
                            id="phone"
                            required
                            value={worker.phone}
                            onChange={handleInputChange}
                            name="phone"
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="title">Email</label>
                        <input
                            type="email"
                            className="form-control"
                            id="email"
                            required
                            value={worker.email}
                            onChange={handleInputChange}
                            name="email"
                        />
                    </div>

                    <button onClick={saveWorker} className="btn btn-success">
                        Submit
                    </button>
                </div>
            )}
        </div>
    );
};

export default AddWorker;
