import React, { useState, useEffect } from "react";
import WorkerService from "../../services/worker.service";

const Worker = props => {
  const positions = [
    'Chief People Officer',
    'VP of Miscellaneous Stuff',
    'Chief Robot Whisperer',
    'Director of First Impressions',
    'Culture Operations Manager',
    'Director of Ethical Hacking',
    'Software Ninjaneer',
    'Director of Bean Counting',
    'Digital Overlord',
    'Director of Storytelling',
  ].map((position, id) => <option key={id}>{position}</option>);

  const genders =  [
    'Female',
    'Male',
  ].map((gender, id) => <option key={id}>{gender}</option>);

  const initialWorkerState = {
    _id: null,
    name: "",
    gender: "",
    salary: 0,
    position: "",
    phone: "",
    email: "",
  };
  const [currentWorker, setCurrentWorker] = useState(initialWorkerState);
  const [message, setMessage] = useState("");

  const getWorker = id => {
    WorkerService.get(id)
      .then(response => {
        setCurrentWorker(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  useEffect(() => {
    getWorker(props.match.params.id);
  }, [props.match.params.id]);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCurrentWorker({ ...currentWorker, [name]: value });
  };



  const updateWorker = () => {
    WorkerService.update(currentWorker._id, currentWorker)
      .then(response => {
        console.log(response.data);
        setMessage("The worker was updated successfully!");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const deleteWorker = () => {
    WorkerService.remove(currentWorker._id)
      .then(response => {
        console.log(response.data);
        props.history.push("/workers");
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <div>
      {currentWorker ? (
        <div className="edit-form">
          <h4>Worker</h4>
          <form>
              <div className="form-group">
                <label htmlFor="title">Name</label>
                <input
                    type="text"
                    className="form-control"
                    id="name"
                    required
                    value={currentWorker.name}
                    onChange={handleInputChange}
                    name="name"
                />
              </div>
              <div className="form-group">
                <label htmlFor="description">Gender({currentWorker.gender})</label>
                <select
                    className="form-control"
                    id="gender"
                    required
                    value={currentWorker.gender}
                    onChange={handleInputChange}
                    name="gender"
                >{genders}
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="title">Salary</label>
                <input
                    type="number"
                    className="form-control"
                    id="salary"
                    required
                    value={currentWorker.salary}
                    onChange={handleInputChange}
                    name="salary"
                />
              </div>
              <div className="form-group">
                <label htmlFor="description">Position</label>
                <select
                    className="form-control"
                    id="position"
                    required
                    value={currentWorker.position}
                    onChange={handleInputChange}
                    name="position"
                >{positions}
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="title">Phone</label>
                <input
                    type="tel"
                    className="form-control"
                    id="phone"
                    required
                    value={currentWorker.phone}
                    onChange={handleInputChange}
                    name="phone"
                />
              </div>
              <div className="form-group">
                <label htmlFor="title">Email</label>
                <input
                    type="email"
                    className="form-control"
                    id="email"
                    required
                    value={currentWorker.email}
                    onChange={handleInputChange}
                    name="email"
                />
              </div>
          </form>



          <button className="badge badge-danger mr-2" onClick={deleteWorker}>
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={updateWorker}
          >
            Update
          </button>
          <p>{message}</p>
        </div>
      ) : (
        <div>
          <br />
          <p>Please click on a Worker...</p>
        </div>
      )}
    </div>
  );
};

export default Worker;
