import React, { useState, useEffect, useMemo, useRef } from "react";
import WorkerService from "../../services/worker.service";
import { useTable } from "react-table";

const WorkersList = (props) => {
  const [workers, setWorkers] = useState([]);
  const [searchText, setSearchText] = useState("");
  const workersRef = useRef();

  workersRef.current = workers;

  useEffect(() => {
    retrieveWorkers();
  }, []);

  const onChangeSearchText = (e) => {
    const searchText = e.target.value;
    console.log(searchText);
    setSearchText(searchText);
    search();
  };

  const retrieveWorkers = () => {
    WorkerService.getAll()
      .then((response) => {
        setWorkers(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const refreshList = () => {
    retrieveWorkers();
  };
  const addWorker = () => {
    props.history.push("/workers/add");
  }

  const removeAllWorkers = () => {
    WorkerService.removeAll()
      .then((response) => {
        console.log(response.data);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const search = () => {
    WorkerService.search(searchText)
      .then((response) => {
        setWorkers(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const openWorker = (rowIndex) => {
    const id = workersRef.current[rowIndex]._id;
    props.history.push("/workers/" + id);
  };

  const deleteWorker = (rowIndex) => {
    const id = workersRef.current[rowIndex]._id;

    WorkerService.remove(id)
      .then((response) => {
        props.history.push("/workers");

        let newWorkers = [...workersRef.current];
        newWorkers.splice(rowIndex, 1);

        setWorkers(newWorkers);
      })
      .catch((e) => {
        console.log(e);
      });
  };


  const columns = useMemo(
    () => [
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Gender",
        accessor: "gender",
      },
      {
        Header: "Salary",
        accessor: "salary",
      },{
        Header: "Position",
        accessor: "position",
      },
      {
        Header: "Phone",
        accessor: "phone",
      },
      {
        Header: "Email",
        accessor: "email",
      },
      {
        Header: "Actions",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.id;
          return (
            <div>
              <span onClick={() => openWorker(rowIdx)}>
                <i className="far fa-edit action "></i>
                  <span className="mr-2">Open</span>
              </span>

              <span onClick={() => deleteWorker(rowIdx)}>
                <span>Delete</span>
              </span>
            </div>
          );
        },
      },
    ],
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data: workers,
  });

  return (
    <div className="list row">
      <div className="col-md-8">
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Search by title"
            value={searchText}
            onChange={onChangeSearchText}
            onKeyUp={onChangeSearchText}
          />
          <div className="input-group-append">
            <button
              className="btn btn-outline-secondary"
              type="button"
              onClick={search}
            >
              Search
            </button>
          </div>
        </div>
      </div>
      <div className="col-md-12 list">
        <table
          className="table table-striped table-bordered"
          {...getTableProps()}
        >
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th {...column.getHeaderProps()}>
                    {column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      <div className="col-md text-right">
        {/*<button className="btn btn-sm btn-danger mr-2" onClick={removeAllWorkers}>*/}
        {/*  Remove All*/}
        {/*</button>*/}
        <button className="btn btn-sm btn-outline-success" onClick={addWorker}>
          Add
        </button>
      </div>
    </div>
  );
};

export default WorkersList;
