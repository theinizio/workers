import React, { useState, useEffect } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import PrivateRoute from "./components/PrivateRoute";

import AuthService from "./services/auth.service";

import Login from "./components/Login";
import WorkersList from "./components/Workers/WorkersList";
import Worker from "./components/Workers/Worker";
import AddWorker from "./components/Workers/AddWorker";

const App = () => {
  const [currentUser, setCurrentUser] = useState(undefined);

  useEffect(() => {
    const user = AuthService.getCurrentUser();

    if (user) {
      setCurrentUser(user);
    }
  }, []);

  const logOut = () => {
    AuthService.logout();
  };

  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <div className="navbar-nav mr-auto">
          {currentUser && (
          <li className="nav-item">
            <Link to={"/workers"} className="nav-link">
              Workers
            </Link>
          </li>)}
        </div>

        {currentUser ? (
          <div className="navbar-nav ml-auto">
            <li className="nav-item">
              <a href="/login" className="nav-link" onClick={logOut}>
                LogOut
              </a>
            </li>
          </div>
        ) : (
          <div className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link to={"/login"} className="nav-link">
                Login
              </Link>
            </li>

          </div>
        )}
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path="/login" component={Login} />
          <PrivateRoute exact path={["/", "/workers"]} component={WorkersList} />
          <PrivateRoute exact path="/workers/add" component={AddWorker} />
          <PrivateRoute path="/workers/:id" component={Worker} />
        </Switch>
      </div>
    </div>
  );
};

export default App;
