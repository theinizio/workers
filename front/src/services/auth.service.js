import axios from "axios";

const API_URL = "http://localhost/api/auth/";

const login = async (username, password) => {
  try {

    const response = await axios
        .post(API_URL + "login", {
          username,
          password,
        })

    if (response.data.access_token) {
      localStorage.setItem("user", JSON.stringify(response.data));
    }

    return response.data;

  } catch (e) {
    console.log(e);
  }
};

const logout = () => {
  localStorage.removeItem("user");
};

const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem("user"));
};

const isLoggedIn = () => {
  const user = getCurrentUser();
  return user && typeof user.access_token !== 'undefined';
};

const getCurrentUserToken = () => {
  const user = getCurrentUser();
  return isLoggedIn() ? user.access_token : null;
};


export default {
  login,
  logout,
  isLoggedIn,
  getCurrentUser,
  getCurrentUserToken,
};
