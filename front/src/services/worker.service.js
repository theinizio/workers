import axios from "axios";
import AuthService from '../services/auth.service';

const http = axios.create({
  baseURL: "http://localhost/api",
  headers: {
    "Authorization": "Bearer " + AuthService.getCurrentUserToken(),
    "Content-type": "application/json"
  }
});

const getAll = () => {
  return http.get("/workers");
};

const get = (id) => {
  return http.get(`/workers/${id}`);
};

const create = (data) => {
  return http.post("/workers", data);
};

const update = (id, data) => {
  return http.patch(`/workers/${id}`, data);
};

const remove = (id) => {
  return http.delete(`/workers/${id}`);
};

const removeAll = () => {
  return http.delete(`/workers`);
};

const search = (search) => {
  return http.get(`/workers?search=${search}`);
};

const WorkerService = {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  search: search,
};

export default WorkerService;
