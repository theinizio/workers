import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as bcrypt from 'bcrypt';

export type WorkerDocument = Worker & Document;

@Schema()
export class Worker {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  gender: string;

  @Prop({ required: true, unique: true })
  phone: string;

  @Prop({ required: true, unique: true })
  email: string;

  @Prop({ required: true})
  addedAt: Date;

  @Prop({required: true})
  position: string;

  @Prop({ required: true })
  salary: number;



}


export const WorkerSchema = SchemaFactory.createForClass(Worker);
