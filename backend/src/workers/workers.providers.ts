import { Connection } from 'mongoose';
import {WorkerSchema} from "./schemas/worker.schema";


export const workersProviders = [
    {
        provide: 'WORKER_MODEL',
        useFactory: (connection: Connection) => connection.model('Worker', WorkerSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];