import {Module} from '@nestjs/common';
import {WorkersController} from './workers.controller';
import {WorkersService} from './workers.service';
import {workersProviders} from './workers.providers'
import {DatabaseModule} from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [WorkersController],
  providers: [
    WorkersService,

    ...workersProviders],
  exports: [WorkersService],
})
export class WorkersModule {
}

