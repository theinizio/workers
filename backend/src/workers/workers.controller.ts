import {Controller, Get, Post, Body, Patch, Param, Delete, HttpCode, UseGuards, Query} from '@nestjs/common';
import { WorkersService } from './workers.service';
import { CreateWorkerDto } from './dto/create-worker.dto';
import { UpdateWorkerDto } from './dto/update-worker.dto';
import {ApiBearerAuth, ApiTags} from '@nestjs/swagger'
import {JwtAuthGuard} from "../auth/jwt-auth.guard";
import {Worker} from "./interfaces/worker.interface";

@ApiTags('Workers')
@Controller('workers')
export class WorkersController {
  constructor(private readonly workersService: WorkersService) {}

  @HttpCode(201)
  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  create(@Body() createWorkerDto: CreateWorkerDto) {
    return this.workersService.create(createWorkerDto);
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findOne(@Param('id') id: string) {
    return this.workersService.findOne(id);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  async findAll(@Query() query) {
    if(query.search) {
      return this.workersService.search(query.search);
    }
    return this.workersService.findAll();
  }
  @HttpCode(200)
  @Patch(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  update(@Param('id') id: string, @Body() updateWorkerDto: UpdateWorkerDto)  {
    return this.workersService.update(id, updateWorkerDto);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  remove(@Param('id') id: string) {
    return this.workersService.remove(id);
  }
}
