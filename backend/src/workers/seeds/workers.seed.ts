import {Command, Positional} from 'nestjs-command';
import {Injectable} from '@nestjs/common';
import {WorkersService} from "../workers.service";
import * as faker from 'faker';

@Injectable()
export class WorkerSeed {
  constructor(
    private readonly workersService: WorkersService,
  ) {
  }

  @Command({command: 'create:workers', describe: 'creates workers', autoExit: true})
  async create() {
    console.log('creating...');
    for (let i = 0; i < 50; i++) {
      console.log(await this.workersService.create({
        name: faker.name.findName(),
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        addedAt: faker.date.recent(),
        salary: Math.ceil(Math.random() * 10000),
        position: faker.helpers.randomize([
          'Chief People Officer',
          'VP of Miscellaneous Stuff',
          'Chief Robot Whisperer',
          'Director of First Impressions',
          'Culture Operations Manager',
          'Director of Ethical Hacking',
          'Software Ninjaneer',
          'Director of Bean Counting',
          'Digital Overlord',
          'Director of Storytelling',
        ]),
        gender: faker.helpers.randomize(['Male', 'Female']),
      }));
    }

  }
}