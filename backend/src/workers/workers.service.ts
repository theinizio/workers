import {Model} from 'mongoose';
import {Inject, Injectable} from '@nestjs/common';
import {CreateWorkerDto} from './dto/create-worker.dto';
import {UpdateWorkerDto} from './dto/update-worker.dto';
import {Worker} from "./interfaces/worker.interface";


@Injectable()
export class WorkersService {

    constructor(
        @Inject('WORKER_MODEL')
        private readonly workerModel: Model<Worker>,
    ) {}


    async create(createWorkerDto: CreateWorkerDto): Promise<Worker> {
        const createdWorker = new this.workerModel(createWorkerDto);
        return createdWorker.save();
    }

    async search(search:string): Promise<Worker[]> {
        const regex = new RegExp(`${search}`, 'gi');
        const filters = [
            { 'name':  regex },
            { 'position': regex },
            { 'gender': regex },
            { 'phone': regex },
            { 'email': { $regex:regex }},
        ];




        return this.workerModel.find({$or: filters}).exec();
    }
 async findAll(): Promise<Worker[]> {
        return this.workerModel.find().exec();
    }

    async findOne(id: string): Promise<Worker | undefined> {
        return this.workerModel.findOne({_id: id}).exec();
    }

    async findByEmailOrPassword(login: string): Promise<Worker | undefined> {
        return this.workerModel.findOne({$or:[{email: login},{phone: login}]}).exec();
    }

    async update(id: string, updateWorkerDto: UpdateWorkerDto) {
        return this.workerModel.updateOne(updateWorkerDto)
    }

    async remove(id: string) {
        return this.workerModel.find({_id: id}).remove();
    }

    async checkPassword(password: string) {
        return true;
    }


}
