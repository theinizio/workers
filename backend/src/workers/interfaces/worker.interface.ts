import { Document } from 'mongoose';

export interface Worker extends Document {
  readonly _id: string;
  readonly name: string;
  readonly gender: string;
  readonly salary: number;
  readonly position: string;
  readonly phone: string;
  readonly email: string;
  readonly addedAt: Date;
}