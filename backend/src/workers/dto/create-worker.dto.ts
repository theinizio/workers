
export class CreateWorkerDto {
    readonly name: string;
    readonly gender: string;
    readonly phone: string;
    readonly email: string;
    readonly addedAt: Date;
    readonly salary: number;
    readonly position: string;
}

