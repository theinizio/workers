import { Test } from '@nestjs/testing';
import { WorkersController } from './workers.controller';
import { WorkersService } from './workers.service';
import { Worker } from './interfaces/worker.interface';
import {Model} from "mongoose";
import {getModelToken} from "@nestjs/mongoose";



describe('WorkersController', () => {
  let workersController: WorkersController;
  let workersService: WorkersService;
  let workersModel: Model<Worker>;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [WorkersController],
      providers: [WorkersService],
    }).compile();

    workersService = moduleRef.get<WorkersService>(WorkersService);
    workersController = moduleRef.get<WorkersController>(WorkersController);
  });

  describe('findAll', () => {
    it('should return an array of workers', async () => {
      const result = [{
          _id: '60a182b8a5f71e079261ecb7',
          name: 'Jim Predovic',
          phone: '1-265-949-1443 x87342',
          email: 'Lia_Fisher22@yahoo.com',
          password: '$2b$10$mCP7FJXbRn.C2e8rCxGP8er0jgQMHmfSGEOmU4Qj7HUOGnDH.n5JW',
          addedAt: new Date(),
        salary: 8082,
        position: 'director',
        __v: 0
    }];
      jest.spyOn(workersService, 'findAll').mockImplementation(() => result);

      expect(await workersController.findAll()).toBe(result);
    });
  });
});
