import { Module } from '@nestjs/common';
import { CommandModule } from 'nestjs-command';
import {UserSeed} from "../users/seeds/users.seed";
import {UsersModule} from "../users/users.module";
import {WorkerSeed} from "../workers/seeds/workers.seed";
import {WorkersModule} from "../workers/workers.module";

@Module({
  imports: [CommandModule, UsersModule, WorkersModule],
  providers: [UserSeed, WorkerSeed],
  exports: [UserSeed, WorkerSeed],
})
export class SeedsModule {}