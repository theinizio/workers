import { Command, Positional } from 'nestjs-command';
import { Injectable } from '@nestjs/common';
import {UsersService} from "../users.service";



@Injectable()
export class UserSeed {
  constructor(
    private readonly usersService: UsersService,
  ) { }

  @Command({ command: 'create:users', describe: 'creates a user', autoExit: true })
  async create() {
    console.log('creating...');
    console.log(await this.usersService.create({
      username: 'user@test.com',
      password: '123456789',
    }));
    console.log(await this.usersService.create({
      username: 'admin@test.com',
      password: '123456789',
    }));
  }
}