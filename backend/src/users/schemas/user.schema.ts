import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from "mongoose";
import * as bcrypt from 'bcrypt';


export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true })
  username: string;

  @Prop({ required: true })
  password: string;

}


export const UserSchema = SchemaFactory.createForClass(User);




UserSchema.pre('save', function(next) {
  var self = this;
  const saltOrRounds = 10;

  // only hash the password if it has been modified (or is new)
  if (!this.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(saltOrRounds, (err, salt) => {
    if (err) return next(err);
    // hash the password using our new salt
    bcrypt.hash(self['password'], salt, (err, hash) => {
      if (err) return next(err);
      // override the cleartext password with the hashed one
      self["password"] = hash;
      next();
    });
  });
});
