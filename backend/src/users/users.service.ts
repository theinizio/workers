import {Model} from 'mongoose';
import {Inject, Injectable} from '@nestjs/common';
import {UserDto} from './dto/user.dto';
import {User} from "./interfaces/user.interface";


@Injectable()
export class UsersService {
    constructor(
        @Inject('USER_MODEL')
        private readonly userModel: Model<User>,
    ) {}

    async create(createUserDto: UserDto): Promise<User> {
        const createdUser = new this.userModel(createUserDto);
        return createdUser.save();
    }

    async findAll(): Promise<User[]> {
        return this.userModel.find().exec();
    }

    async findOne(id: string): Promise<User | undefined> {
        return this.userModel.findOne({_id: id}).exec();
    }

    async findByUsername(username: string): Promise<User | undefined> {
        return this.userModel.findOne({username: username}).exec();
    }

    async remove(id: string) {
        return this.userModel.find({_id: id}).remove();
    }

}
