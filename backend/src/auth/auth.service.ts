import {Injectable} from '@nestjs/common';
import {UsersService} from '../users/users.service';
import {JwtService} from '@nestjs/jwt';
import * as bcrypt from "bcrypt";
import {User} from "../users/interfaces/user.interface";

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService
    ) {
    }

    async validateUser(login: string, pass: string): Promise<User|null> {
        const user = await this.usersService.findByUsername(login);
        if (user && await bcrypt.compare(pass, user.password)) {
            return user;
        }
        return null;
    }

    async login(user: User) {
        const payload = {username: user.username, sub: user._id};

        return {
            access_token: this.jwtService.sign(payload),
            username: payload.username,
        };
    }
}