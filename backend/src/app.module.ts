import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from './users/users.module';
import {DatabaseModule} from "./database/database.module";
import {usersProviders} from "./users/users.providers";
import { AuthModule } from './auth/auth.module';
import {SeedsModule} from "./shared/seeds.module";
import {CommandModule} from "nestjs-command";
import {WorkersModule} from "./workers/workers.module";

@Module({
  imports: [
    CommandModule,
    DatabaseModule,
    SeedsModule,
    ConfigModule.forRoot({
      isGlobal: true
    }),
    UsersModule,
    WorkersModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,

    ...usersProviders
  ],
})
export class AppModule {}
