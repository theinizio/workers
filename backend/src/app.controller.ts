import {Controller, Request, Post, UseGuards, Body} from '@nestjs/common';
import {LocalAuthGuard} from "./auth/local-auth.guard";
import {ApiTags} from '@nestjs/swagger'
import {AuthService} from "./auth/auth.service";
import {UsersService} from "./users/users.service";
import {UserDto} from "./users/dto/user.dto";

@ApiTags('login')
@Controller()
export class AppController {
  constructor(private authService: AuthService, private readonly usersService: UsersService) {
  }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req, @Body() userDto:UserDto) {
    return this.authService.login(req.user);
  }

  @Post('auth/register')
  async register(@Body() createUserDto: UserDto) {
    const user = await this.usersService.create(createUserDto);
    return this.authService.login(user);
  }


}
