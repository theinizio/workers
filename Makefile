include .env


up: # create and start containers
	@docker-compose -f ${DOCKER_CONFIG} up -d

down: # stop and destroy containers
	@docker-compose -f ${DOCKER_CONFIG} down

down-volume: #  WARNING: stop and destroy containers with volumes
	@docker-compose -f ${DOCKER_CONFIG} down -v

start: # start already created containers
	@docker-compose -f ${DOCKER_CONFIG} start

stop: # stop containers, but not destory
	@docker-compose -f ${DOCKER_CONFIG} stop

ps: # show started containers and their status
	@docker-compose -f ${DOCKER_CONFIG} ps

build: # build all dockerfile, if not built yet
	@docker-compose -f ${DOCKER_CONFIG} build


connect_backend: # app command line
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www nest sh

connect_front: # app command line
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www react sh

connect_nginx: # nginx command line
	@docker-compose -f ${DOCKER_CONFIG} exec -w /www nginx sh

connect_db: # database command line
	@docker-compose -f ${DOCKER_CONFIG} exec db sh



node_Modules: # npm install
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www nest npm install
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www react npm install


populete_db: # refresh the database and run all database seeds
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www nest sh -c "npx nestjs-command create:users"
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www nest sh -c "npx nestjs-command create:workers"

test: # run all tests
	@docker-compose -f ${DOCKER_CONFIG} exec -u www -w /www nest sh -c "npm run test"

reload_nginx: # refresh the jwt secret
	@docker-compose -f ${DOCKER_CONFIG} exec -w /www nginx nginx -s reload

watch_nest_logs:
	@docker-compose -f 	${DOCKER_CONFIG} logs --follow nest